package com.example.rentit;


import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class RentItApplicationTests {

    @Test
    void contextLoads() {
    }

}
