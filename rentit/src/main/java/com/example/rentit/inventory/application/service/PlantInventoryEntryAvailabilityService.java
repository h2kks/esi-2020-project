package com.example.rentit.inventory.application.service;

import com.example.rentit.inventory.domain.repository.CustomPlantInventoryEntryRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class PlantInventoryEntryAvailabilityService {

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Qualifier("customPlantInventoryEntryRepositoryImpl")
    @Autowired
    CustomPlantInventoryEntryRepository customPlantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public boolean isAvailable(long id, LocalDate startDate, LocalDate endDate) {
        return customPlantInventoryEntryRepository.getEntryAvailabilityById(id, startDate, endDate);
    }
}
