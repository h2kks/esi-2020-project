package com.example.rentit.inventory.domain.repository;

import com.example.rentit.inventory.domain.model.PlantInventoryEntry;

import java.time.LocalDate;
import java.util.List;

public interface CustomPlantInventoryEntryRepository {
    boolean getEntryAvailabilityById(Long id, LocalDate startDate, LocalDate endDate);

    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
}
