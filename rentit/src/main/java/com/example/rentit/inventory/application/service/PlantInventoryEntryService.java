package com.example.rentit.inventory.application.service;

import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PlantInventoryEntryService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public CollectionModel<PlantInventoryEntryDTO> all() {
        return plantInventoryEntryAssembler.toCollectionModel(plantInventoryEntryRepository.findAll());
    }

    public CollectionModel<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> entries = plantInventoryEntryRepository.findAvailablePlants(name, startDate, endDate);
        return plantInventoryEntryAssembler.toCollectionModel(entries);
    }

    public PlantInventoryEntry findEntryById(Long id) {
        return plantInventoryEntryRepository.findById(id).orElse(null);
    }

    public PlantInventoryEntryDTO findEntryDTOById(Long id) {
        return plantInventoryEntryAssembler.toModel(plantInventoryEntryRepository.findById(id).orElse(null));
    }

}
