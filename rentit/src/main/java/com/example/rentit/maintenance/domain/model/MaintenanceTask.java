package com.example.rentit.maintenance.domain.model;

import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class MaintenanceTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork type_of_work;

    @Column(precision = 8, scale = 2)
    BigDecimal price;

    @Embedded
    BusinessPeriod maintenancePeriod;

    @ManyToOne
    PlantInventoryItem reservation;

}
