package com.example.rentit.maintenance.rest;

import com.example.rentit.maintenance.application.dto.MaintenanceTaskCreationBody;
import com.example.rentit.maintenance.application.dto.MaintenanceTaskDTO;
import com.example.rentit.maintenance.application.services.MaintenanceTaskService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/maintenance/tasks")
public class MaintenanceTaskRestController {

    @Autowired
    private MaintenanceTaskService maintenanceTaskService;

    @PostMapping
    public Pair<String, MaintenanceTaskDTO> create(@RequestBody MaintenanceTaskCreationBody body) throws ResponseStatusException {
        try {
            return maintenanceTaskService.createMaintenanceTask(body);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PutMapping("/{taskId}")
    public MaintenanceTaskDTO update(@PathVariable Long taskId, @RequestBody MaintenanceTaskDTO dto) throws Exception {
        if (dto.get_id() == null) {
            dto.set_id(taskId);
        }
        return maintenanceTaskService.updateMaintenanceTask(dto);
    }

    @GetMapping("")
    public CollectionModel<MaintenanceTaskDTO> get() {
        return maintenanceTaskService.all();
    }

    @GetMapping("/{id}")
    public MaintenanceTaskDTO fetchMaintenanceTask(@PathVariable("id") Long id) {
        try {
            return maintenanceTaskService.findMaintenanceTask(id);
        } catch (Exception e) {
            if (e.getMessage() == "Maintenance task does not exist") {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error: could not find maintenance task with id", e);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: something went wrong", e);
            }
        }
    }

}