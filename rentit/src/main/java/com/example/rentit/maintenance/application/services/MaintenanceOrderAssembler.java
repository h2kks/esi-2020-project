package com.example.rentit.maintenance.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.service.PlantInventoryItemAssembler;
import com.example.rentit.maintenance.application.dto.MaintenanceOrderDTO;
import com.example.rentit.maintenance.domain.model.MaintenanceOrder;
import com.example.rentit.maintenance.rest.MaintenanceOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class MaintenanceOrderAssembler extends RepresentationModelAssemblerSupport<MaintenanceOrder, MaintenanceOrderDTO> {

    public MaintenanceOrderAssembler() {
        super(MaintenanceOrderRestController.class, MaintenanceOrderDTO.class);
    }

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Override
    public MaintenanceOrderDTO toModel(MaintenanceOrder maintenanceOrder) {
        MaintenanceOrderDTO dto = createModelWithId(maintenanceOrder.getId(), maintenanceOrder);
        dto.set_id(maintenanceOrder.getId());
        dto.setStatus(maintenanceOrder.getStatus());
        dto.setMaintenancePeriod(BusinessPeriodDTO.of(maintenanceOrder.getMaintenancePeriod().getStartDate(), maintenanceOrder.getMaintenancePeriod().getEndDate()));
        dto.setPlant(plantInventoryItemAssembler.toModel(maintenanceOrder.getPlant()));
        dto.setSite(maintenanceOrder.getSite());
        dto.setDescription(maintenanceOrder.getDescription());
        dto.setEngineerName(maintenanceOrder.getEngineerName());

        dto.add(linkTo(methodOn(MaintenanceOrderRestController.class).fetchMaintenanceOrder(dto.get_id())).withRel("fetch"));

        return dto;
    }

}