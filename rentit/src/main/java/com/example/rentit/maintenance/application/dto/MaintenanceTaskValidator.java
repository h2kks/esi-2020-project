package com.example.rentit.maintenance.application.dto;

import com.example.rentit.inventory.domain.model.EquipmentCondition;
import com.example.rentit.maintenance.domain.model.MaintenanceTask;
import com.example.rentit.maintenance.domain.model.TypeOfWork;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class MaintenanceTaskValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MaintenanceTask.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MaintenanceTask task = (MaintenanceTask) o;

        if (task.getType_of_work() == null) {
            errors.rejectValue("type_of_work", "type of work cannot be null");
        }
        if (task.getReservation() == null) {
            errors.rejectValue("reservation", "reservation item cannot be null");
        }
        if (StringUtils.isBlank(task.getDescription())) {
            errors.rejectValue("description", "description of work should be provided");
        }
        if (TypeOfWork.PREVENTIVE.equals(task.getType_of_work()) && !EquipmentCondition.SERVICEABLE.equals(task.getReservation().getEquipmentCondition())) {
            errors.rejectValue("type_of_work", "preventive task cannot be performed for a plant that is not SERVICEABLE");
        }

        if (TypeOfWork.CORRECTIVE.equals(task.getType_of_work()) &&
                (!EquipmentCondition.UNSERVICEABLEREPAIRABLE.equals(task.getReservation().getEquipmentCondition()) &&
                        !EquipmentCondition.UNSERVICEABLEINCOMPLETE.equals(task.getReservation().getEquipmentCondition()))) {
            errors.rejectValue("type_of_work", "corrective task cannot be performed for a plant that is not UNSERVICEABLEINCOMPLETE or UNSERVICEABLEREPAIRABLE");
        }

        if (TypeOfWork.OPERATIONAL.equals(task.getType_of_work()) && EquipmentCondition.UNSERVICEABLECONDEMNED.equals(task.getReservation().getEquipmentCondition())) {
            errors.rejectValue("type_of_work", "operational task cannot be performed for a plant with UNSERVICEABLECONDEMNED condition");
        }
    }
}