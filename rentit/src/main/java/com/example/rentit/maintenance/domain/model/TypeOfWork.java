package com.example.rentit.maintenance.domain.model;

public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIONAL;
}
