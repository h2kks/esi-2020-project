package com.example.rentit.invoices.domain.model;

import com.example.rentit.sales.domain.model.PurchaseOrder;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne
    PurchaseOrder purchaseOrder;

    LocalDate dueDate;

    @Column(precision = 8, scale = 2)
    BigDecimal amount;

    @Enumerated(EnumType.STRING)
    InvoiceStatus status;

    public static Invoice of(PurchaseOrder purchaseOrder, LocalDate dueDate, BigDecimal amount) {
        Invoice invoice = new Invoice();
        invoice.setPurchaseOrder(purchaseOrder);
        invoice.setDueDate(dueDate);
        invoice.setAmount(amount);
        return invoice;
    }
}
