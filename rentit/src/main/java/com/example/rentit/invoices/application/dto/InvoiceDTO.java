package com.example.rentit.invoices.application.dto;

import com.example.rentit.invoices.domain.model.InvoiceStatus;
import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class InvoiceDTO extends RepresentationModel<InvoiceDTO> {
    Long _id;
    PurchaseOrderDTO purchaseOrder;
    LocalDate dueDate;
    BigDecimal amount;
    InvoiceStatus status;
}
