// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import Notifications from 'vue-notification'
import Vuetify from 'vuetify'
import VueMoment from 'vue-moment'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import moment from 'moment-timezone'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vuetify/dist/vuetify.min.css'
import '../main.css'
import 'element-ui/lib/theme-chalk/index.css'
import ToggleButton from 'vue-js-toggle-button'

Vue.use(Vuetify)
Vue.use(BootstrapVue)
Vue.use(Notifications)
Vue.use(VueMoment, {moment})
Vue.use(ElementUI, { locale })
Vue.use(ToggleButton)

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
