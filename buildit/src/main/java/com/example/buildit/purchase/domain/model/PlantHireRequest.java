package com.example.buildit.purchase.domain.model;

import com.example.buildit.common.domain.model.BusinessPeriod;
import com.example.buildit.invoices.domain.model.Invoice;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PlantHireRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String siteId;
    String siteEngineerName;
    String supplier;
    String plantReference;
    BigDecimal price;
    String purchaseOrderReference;

    @Enumerated(EnumType.STRING)
    RequestStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @OneToOne
    Invoice invoice;

    public static PlantHireRequest of(String plantReference, BusinessPeriod rentalPeriod, String siteId, String siteEngineerName, String supplier, RequestStatus status, BigDecimal price) {
        PlantHireRequest plantHireRequest = new PlantHireRequest();
        plantHireRequest.plantReference = plantReference;
        plantHireRequest.rentalPeriod = rentalPeriod;
        plantHireRequest.siteId = siteId;
        plantHireRequest.siteEngineerName = siteEngineerName;
        plantHireRequest.supplier = supplier;
        plantHireRequest.status = RequestStatus.PENDING;
        plantHireRequest.price = price;

        return plantHireRequest;
    }

    public BusinessPeriod getRentalPeriod() {
        return rentalPeriod;
    }

    public String getSiteEngineerName() {
        return siteEngineerName;
    }

    public String getSiteId() {
        return siteId;
    }

    public String getSupplier() {
        return supplier;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public BigDecimal getPrice() {
        return price;
    }


    public void setRentalPeriod(BusinessPeriod rentalPeriod) {
        this.rentalPeriod = rentalPeriod;
    }

    public void setSiteEngineerName(String siteEngineerName) {
        this.siteEngineerName = siteEngineerName;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPurchaseOrderReference() {
        return purchaseOrderReference;
    }

    public void setPurchaseOrderReference(String purchaseOrderReference) {
        this.purchaseOrderReference = purchaseOrderReference;
    }
}
