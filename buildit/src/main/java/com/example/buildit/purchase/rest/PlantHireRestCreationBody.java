package com.example.buildit.purchase.rest;

import com.example.buildit.purchase.domain.model.RequestStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PlantHireRestCreationBody {

    private String reference;
    private LocalDate startDate;
    private LocalDate endDate;
    private String siteId;
    private String siteEngineerName;
    private String supplier;
    private RequestStatus status;
    private BigDecimal price;

    public String getSiteEngineerName() {
        return siteEngineerName;
    }

    public void setSiteEngineerName(String siteEngineerName) {
        this.siteEngineerName = siteEngineerName;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "MaintenanceOrderCreationBody{" +
                "plantReference=" + reference +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", siteId='" + siteId + '\'' +
                ", siteEngineerName='" + siteEngineerName + '\'' +
                ", supplier='" + supplier + '\'' +
                ", status='" + status + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
