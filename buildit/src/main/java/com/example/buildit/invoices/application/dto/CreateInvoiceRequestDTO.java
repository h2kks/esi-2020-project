package com.example.buildit.invoices.application.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class CreateInvoiceRequestDTO {
    String purchaseOrderReference;
    LocalDate dueDate;
    BigDecimal amount;
}
