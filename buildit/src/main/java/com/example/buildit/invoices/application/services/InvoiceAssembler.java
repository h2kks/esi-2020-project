package com.example.buildit.invoices.application.services;

import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.invoices.domain.model.Invoice;
import com.example.buildit.purchase.application.service.PlantHireRequestAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class InvoiceAssembler extends RepresentationModelAssemblerSupport<Invoice, InvoiceDTO> {

    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;

    public InvoiceAssembler() {
        super(InvoiceAssembler.class, InvoiceDTO.class);
    }

    @Override
    public InvoiceDTO toModel(Invoice invoice) {
        InvoiceDTO dto = createModelWithId(invoice.getId(), invoice);
        dto.set_id(invoice.getId());
        dto.setPlantRequest(plantHireRequestAssembler.toModel(invoice.getPlantRequest()));
        dto.setDueDate(invoice.getDueDate());
        dto.setAmount(invoice.getAmount());
        dto.setStatus(invoice.getStatus());
        dto.setPurchaseOrderReference(invoice.getPurchaseOrderReference());
        dto.setPaymentDate(invoice.getPaymentDate());
        return dto;
    }
}
